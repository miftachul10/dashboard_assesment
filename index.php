<?php

include "crud/connection.php";

$kuota = 30;
$temp_data = count($data_daftar);
$temp_kosong = $kuota - $temp_data;

?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <script src="https://kit.fontawesome.com/a540d7261a.js" crossorigin="anonymous"></script>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="css/styles.css">
    <title>Dashboard</title>
    <style>
        body{
            overflow-x: hidden;
        }
    </style>
  </head>
  <body>
    <nav class="navbar navbar-expand-lg navbar-light bg-dark">
        <a class="navbar-brand pl-1 text-light" href="#"><h3>Sekolah Teknologi Digital</h3></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link text-light"  href="#"><i class="fas fa-envelope"  style="font-size: 2em; color: white;"></i></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-light" href="#"><i class="far fa-bell"  style="font-size: 2em; color: white;"></i></a>
                </li>
            </ul>
        </div>
    </nav>
        <div class="row">
            <div class="col-12 col-md-2 bg-dark">
                <ul class="nav flex-column">
                    <li class="nav-item">
                        <img src="img/profil.png" alt="" class="rounded-circle ml-5 mt-3" width="75px" height="75px" >
                    </li>
                    <li class="nav-item">
                    <h2 class="ml-4 text-info"><i>Detak IB</i></h2>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " href="index.php" style="font-size: 2em; color: white;"><i class="fas fa-home"></i>Beranda</a><hr class="bg-light">
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " href="home.php" style="font-size: 20px; color: white;"><i class="far fa-calendar-alt"></i> Tentang</a><hr class="bg-light">
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="daftar.php" style="font-size: 20px; color: white;"><i class="fas fa-sign-in-alt"></i> Daftar</a><hr class="bg-light">
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="setting.php" style="font-size: 20px; color: white;"><i class="fas fa-cog"></i> Setting</a><hr class="bg-light">
                    </li>
                </ul>
            </div>
            <div class="col-12 col-md-10">
                <div class="row text-center">
                    <div class="col-12">
                        <h1>Dashboard Daftar</h1>
                    </div>
                    <div class="col-12 col-md-4 ">
                        <div class="card" >
                            <div class="row no-gutters">
                                <div class="col-md-4 bg-dark ">
                                    <i class="fas fa-users mt-4" style="font-size: 4em; color:aliceblue"></i>
                                </div>
                                <div class="col-md-8 bg-secondary text-light">
                                    <div class="card-body">
                                    <h5 class="card-title">Kuota Pendaftaran</h5>
                                    <h3 class="card-text"><?php echo $kuota; ?></h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-4 ">
                        <div class="card" >
                            <div class="row no-gutters">
                                <div class="col-md-4 bg-dark ">
                                    <i class="fas fa-user-plus mt-4" style="font-size: 4em; color:aliceblue"></i>
                                </div>
                                <div class="col-md-8 bg-secondary text-light">
                                    <div class="card-body">
                                    <h5 class="card-title">Jumlah Pendaftaran</h5>
                                    <h3 class="card-text"><?php echo $temp_data; ?></h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-4 ">
                        <div class="card" >
                            <div class="row no-gutters">
                                <div class="col-md-4 bg-dark ">
                                    <i class="fas fa-user-alt-slash mt-4" style="font-size: 4em; color:aliceblue"></i>
                                </div>
                                <div class="col-md-8 bg-secondary text-light">
                                    <div class="card-body">
                                    <h5 class="card-title">Sisa Pendaftaran</h5>
                                    <h3 class="card-text"><?php echo $temp_kosong; ?></h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>                    
                </div>
                <div class="row mt-5">
                    <div class="col-12 col-md-8">
                        <h1 class="text-center">Data Pendaftar</h1>
                        <table class="table table-striped bg-light mt-3">
                            <thead class="bg-secondary">
                                <tr  class="text-light">
                                    <th scope="col">Nama</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Mapel</th>
                                    <th scope="col">Paket</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($data_daftar as $key) : ?>
                                    <tr>
                                        <td><?php echo $key["nama"]; ?></td>
                                        <td><?php echo $key["email"]; ?></td>
                                        <td><?php echo $key["mapel"]; ?></td>
                                        <td><?php echo $key["paket"]; ?></td>                    
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-12 col-md-4">
                    <h1>Daftar sekarang</h1>
                    <div class="mt-3 bg-secondary p-3 text-light">
                    <form action="crud/input.php" method="POST">
                          <div class="form-group">
                            <label for="exampleInputEmail1">Nama</label>
                            <input type="text" autocomplete="off" name="nama" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukkan Nama" required>
                          </div>
                          <div class="form-group">
                            <label for="exampleInputPassword1">Email</label>
                            <input type="text" autocomplete="off" name="email" class="form-control" id="exampleInputPassword1" placeholder="Masukkan Email" required>
                          </div>
                          <div class="form-group">
                            <label for="exampleInputPassword1">Mapel</label>
                            <select name="mapel" class="form-control">
                              <option  value="Matematika">Matematika</option>
                                <option  value="Fisika">Fisika</option>
                                <option  value="Biologi">Biologi</option>
                                <option  value="Bahasa Indonesia">Bahasa Indonesia</option>
                                <option  value="Bahasa Inggris">Bahasa Inggris</option>
                              </select>
                          </div>
                          <div class="form-group">
                            <label for="exampleInputPassword1">Paket</label>
                              <select name="paket" class="form-control">
                                <option  value="Paket A">Paket A</option>
                                <option  value="Paket B">Paket B</option>
                                <option  value="Paket C">Paket C</option>
                              </select>
                            </div>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                      </form>
                    </div>
                    </div>
                </div>
            </div>
        </div>




    <!-- Optional JavaScript; choose one of the two! -->
    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>

    <!-- Option 2: jQuery, Popper.js, and Bootstrap JS
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
    -->
  </body>
</html>