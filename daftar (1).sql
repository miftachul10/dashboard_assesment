-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 18, 2020 at 06:44 AM
-- Server version: 5.7.24
-- PHP Version: 7.2.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `project`
--

-- --------------------------------------------------------

--
-- Table structure for table `daftar`
--

CREATE TABLE `daftar` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `mapel` enum('Matematika','Fisika','Biologi','Bahasa Indonesia','Bahasa Inggris') DEFAULT NULL,
  `paket` enum('Paket A','Paket B','Paket C') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `daftar`
--

INSERT INTO `daftar` (`id`, `nama`, `email`, `mapel`, `paket`) VALUES
(2, 'Miftachul Rohman ', 'miftadrive@gmail.com', 'Matematika', 'Paket A'),
(6, 'Rendi Saputra', 'saputrarendi9966@gmail.com', 'Fisika', 'Paket B'),
(9, 'Toba Fatir', 'toba@gmail.com', 'Matematika', 'Paket A'),
(12, 'Sudiyono', 'yono@gmail.com', 'Biologi', 'Paket C'),
(14, 'devan sanjaya', 'devan@gmail.com', 'Bahasa Indonesia', 'Paket A'),
(16, 'Toba', 'Tobaeti@gmail.com', 'Bahasa Indonesia', 'Paket B'),
(17, 'Fediansah', 'ferdi@gmail.com', 'Biologi', 'Paket B');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `daftar`
--
ALTER TABLE `daftar`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `daftar`
--
ALTER TABLE `daftar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
