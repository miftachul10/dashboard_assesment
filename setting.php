<?php

include "crud/connection.php";
include "crud/search.php";

?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <script src="https://kit.fontawesome.com/a540d7261a.js" crossorigin="anonymous"></script>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="css/styles.css">
    <title>Dashboard</title>
    <style>
    body{
      overflow-x: hidden;
    }
    </style>
  </head>
  <body>
    
  <nav class="navbar navbar-expand-lg navbar-light bg-dark">
        <a class="navbar-brand pl-1 text-light" href="#"><h3>Sekolah Teknologi Digital</h3></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link text-light"  href="#"><i class="fas fa-envelope"  style="font-size: 2em; color: white;"></i></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-light" href="#"><i class="far fa-bell"  style="font-size: 2em; color: white;"></i></a>
                </li>
            </ul>
        </div>
    </nav>
        <div class="row">
            <div class="col-12 col-md-2 bg-dark">
                <ul class="nav flex-column">
                    <li class="nav-item">
                        <img src="img/profil.png" alt="" class="rounded-circle ml-5 mt-3" width="75px" height="75px" >
                    </li>
                    <li class="nav-item">
                    <h2 class="ml-4 text-info"><i>Detak IB</i></h2>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " href="index.php" style="font-size: 2em; color: white;"><i class="fas fa-home"></i>Beranda</a><hr class="bg-light">
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " href="home.php" style="font-size: 20px; color: white;"><i class="far fa-calendar-alt"></i> Tentang</a><hr class="bg-light">
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="daftar.php" style="font-size: 20px; color: white;"><i class="fas fa-sign-in-alt"></i> Daftar</a><hr class="bg-light">
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="setting.php" style="font-size: 20px; color: white;"><i class="fas fa-cog"></i> Setting</a><hr class="bg-light">
                    </li>
                </ul>
            </div>
            <div class="col-12 col-md-10">
            <div class="container " >
          <div class="row">
            <div class="col">
              <h1 class="text-center mt-3" style="font-family: calibry;"><b>Edit and Delete</b></h1>
              <form class="form-inline my-2 my-lg-0 mt-3 " action="data.php" method="POST">
                <input class="form-control mr-sm-2" autocomplete="off" name="search" type="search" placeholder="Search" aria-label="Search">
                <button class="btn btn-secondary my-2 my-sm-0" name="cari" type="submit">cari</button>
              </form>
              <!-- Allert Message -->
              <?php if(isset($row)):?>
              <div class="alert alert-primary alert-dismissible fade show mt-3" role="alert">
                <p class="lead"><?php echo $row; ?>Data Ditemukan</p>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <?php endif;?>
              <table class="table table-striped bg-light mt-3  border border-secondary">
                <thead class="bg-secondary">
                  <tr>
                    <th scope="col" class="text-light">Nama</th>
                    <th scope="col" class="text-light">Email</th>
                    <th scope="col" class="text-light">Mapel</th>
                    <th scope="col" class="text-light">Paket</th>
                    <th scope="col" class="text-light">Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($data_daftar as $key) : ?>
                    <tr>
                      <td><?php echo $key["nama"]; ?></td>
                      <td><?php echo $key["email"]; ?></td>
                      <td><?php echo $key["mapel"]; ?></td>
                      <td><?php echo $key["paket"]; ?></td>  
                      <td><a class="btn btn-outline-danger" onclick="return confirm ('apakah anda ingin mengapus data ini?')" href="crud/delete.php?id=<?php echo $key["id"]; ?>"><i class="fas fa-user-minus mt-2"></i></a>
                      <a class="btn btn-outline-success" href="crud/edit.php?id=<?php echo $key["id"]; ?>"><i class="fas fa-user-edit mt-2"></i></a></td>                  
                    </tr>
                    <?php endforeach; ?>
                </tbody>
              </table>                            
            </div>
          </div>
        </div>  
            </div>
        </div>




    <!-- Optional JavaScript; choose one of the two! -->
    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>

    <!-- Option 2: jQuery, Popper.js, and Bootstrap JS
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
    -->
  </body>
</html>